<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->string('slug', 192)->unique();
            $table->boolean('enabled')->default(false)->index();
            $table->boolean('featured')->default(false)->index();
            $table->date('publish_start');
            $table->date('publish_end')->nullable();
            $table->unsignedBigInteger('hits')->default(0)->index();
            $table->text('heading')->nullable();
            $table->text('abstract')->nullable();
            $table->longText('content')->nullable();
            $table->longText('parts')->nullable();
            $table->text('details');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
