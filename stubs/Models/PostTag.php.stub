<?php

namespace App;

use FileTools\HasFiles;
use ColorTools\HasImages;
use Wai\Adminify\Traits\Loggable;
use Wai\Adminify\Traits\HasThumbnail;
use Wai\Adminify\Traits\DynamicChangeAppends;
use Wai\AdminifyWysiwyg\Models\PostTag as Model;

class PostTag extends Model
{
    use Loggable;
    use HasFiles;
    use HasImages;
    use HasThumbnail;
    use DynamicChangeAppends;

    protected $thumbnail_column = 'name';

    protected $table = 'post_tags';

    protected $hidden = ['pivot'];

    protected $casts = [
        'enabled' => 'boolean'
    ];

    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    public function getDetailsAttribute()
    {
        $details = json_decode($this->attributes['details'], true);

        if (!isset($details['description'])) $details['description'] = '';

        if (!isset($details['seo']['title'])) $details['seo']['title'] = '';
        if (!isset($details['seo']['description'])) $details['seo']['description'] = '';
        if (!isset($details['seo']['keywords'])) $details['seo']['keywords'] = '';

        return $details;
    }

}
