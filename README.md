# Admin package for Laravel

This package will configure wysiwyg extension for Vuetify admin panel.

## Installation

You can install this package via composer using this command:

```bash
composer require "wai/adminify-wysiwyg"
```

The package will automatically register itself.

## Upgrading

Please see [UPGRADING](UPGRADING.md) for details.

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
