<?php

namespace Wai\AdminifyWysiwyg;

use DateTime;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AdminifyWysiwygServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishesMigration('CreatePostsTable', 'create_posts_table', 1);
        $this->publishesMigration('CreatePostAuthorsTable', 'create_post_authors_table', 2);
        $this->publishesMigration('CreatePostCategoriesTable', 'create_post_categories_table', 3);
        $this->publishesMigration('CreatePostTagsTable', 'create_post_tags_table', 4);
        $this->publishesMigration('CreatePostAssociationsPivotTable', 'create_post_associations_pivot_table', 5);

        $this->publishesModel('Post');
        $this->publishesModel('PostAuthor');
        $this->publishesModel('PostCategory');
        $this->publishesModel('PostTag');

        $this->registerRoutes();
    }

    public function register()
    {

    }

    protected function registerRoutes()
    {
        Route::middleware(['web', 'auth'])->group(function () {
            Route::post('/resources/getPostAuthors', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\ResourcesController@getPostAuthors']);
            Route::post('/resources/getPostCategories', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\ResourcesController@getPostCategories']);
            Route::post('/resources/getPostTags', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\ResourcesController@getPostTags']);

            Route::post('/posts/authors/{any?}', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\PostAuthorsController@vueHelper', 'as' => 'posts.authors']);
            Route::post('/posts/categories/{any?}', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\PostCategoriesController@vueHelper', 'as' => 'posts.categories']);
            Route::post('/posts/tags/{any?}', ['uses' => '\Wai\AdminifyWysiwyg\Controllers\PostTagsController@vueHelper', 'as' => 'posts.tags']);

            Route::post('/posts/{any}', ['uses' => 'App\Http\Controllers\PostsController@vueHelper', 'as' => 'posts']);
        });
    }

    protected function publishesMigration(string $className, string $fileName, int $timestampSuffix)
    {
        if (!class_exists($className)) {
            $timestamp = (new DateTime())->format('Y_m_d_His') . $timestampSuffix;
            $this->publishes([
                __DIR__ . "/../stubs/{$fileName}.php.stub" => database_path('migrations/' . $timestamp . "_{$fileName}.php"),
            ], 'migrations');
        }
    }

    protected function publishesModel(string $className)
    {
        if (!class_exists($className)) {
            $this->publishes([
                __DIR__ . "/../stubs/Models/{$className}.php.stub" => app_path("{$className}.php"),
            ], 'model');
        }
    }
}
