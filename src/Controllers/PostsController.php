<?php

namespace Wai\AdminifyWysiwyg\Controllers;

use App\Log;
use App\PostAuthor;
use App\PostCategory;
use App\PostTag;
use App\Role;
use App\Post;
use App\Notification;
use App\Services\Tabs;
use Illuminate\Support\Str;
use Wai\Adminify\Events\UpdateDom;
use Wai\Adminify\Events\CacheClear;
use App\Http\Controllers\FilesController;
use App\Http\Controllers\ImagesController;
use Wai\Adminify\Controllers\VueController;

class PostsController extends VueController
{
    protected $class = Post::class;
    protected $itemName = 'post';
    protected $orderAsc = false;

    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = ['thumbnail', 'cover', 'author_ids', 'category_ids', 'tag_ids', 'permissions', 'tags'];
    protected $multipleAppends = ['thumbnail', 'authors_count', 'categories_count', 'tags_count'];

    protected $singleRelationships = ['images'];
    protected $multipleRelationships = [];

    protected $searchColumns = ['title'];

    /**
     * Custom search query for users.
     *
     * @param bool $trashedOnly
     * @return mixed
     */
    public function searchQuery($trashedOnly = false)
    {
        $filter = request('filter');
        $itemsPerPage = request('itemsPerPage');

        if ($itemsPerPage) {
            $this->itemsPerPage = $itemsPerPage;
        }

        $results = $this->class::query();

        if ($trashedOnly) {
            $results->onlyTrashed();
        }

        $results->with($this->multipleRelationships);

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $this->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (isset($filter['filters']) and !empty($filter['filters'])) {
            $this->applyFilters($results, $filter['filters']);
        }

        $this->checkSortBy($results);

        return $results;
    }

    /**
     * Specify which table columns to search for text and return result query.
     *
     * @param $query
     * @param $searchQuery
     * @return mixed
     */
    public function searchColumnsForText($query, $searchQuery)
    {
        $searchTerms = explode(' ', $searchQuery);
        $searchColumns = $this->searchColumns;

        $authors = [];
        $categories = [];
        $tags = [];
        $searchWords = [];

        foreach ($searchTerms as $searchTerm) {
            if (strpos($searchTerm, '@') === 0) {
                $authors[] = $searchTerm;
            } elseif (strpos($searchTerm, '#') === 0) {
                $tags[] = $searchTerm;
            } elseif (strpos($searchTerm, '>') === 0) {
                $categories[] = $searchTerm;
            } else {
                $searchWords[] = $searchTerm;
            }
        }

        $query->where(function ($sq) use ($authors, $categories, $tags) {
            if (!empty($authors)) {
                $authorQuery = PostAuthor::query();

                $first = true;
                foreach ($authors as $author) {
                    $author = '%' . trim(substr($author, 1)) . '%';
                    if ($first) {
                        $authorQuery->where('first_name', 'LIKE', $author);
                        $authorQuery->orWhere('last_name', 'LIKE', $author);
                        $first = false;
                    } else {
                        $authorQuery->orWhere('first_name', 'LIKE', $author);
                        $authorQuery->orWhere('last_name', 'LIKE', $author);
                    }
                }

                $authorIds = $authorQuery->pluck('id');

                $sq->where(function ($subQuery) use ($authorIds) {
                    $subQuery->whereHas('authors', function ($q) use ($authorIds) {
                        $q->whereIn('id', $authorIds);
                    });
                });
            }

            if (!empty($categories)) {
                $categoryQuery = PostCategory::query();

                $first = true;
                foreach ($categories as $category) {
                    $category = '%' . trim(substr($category, 1)) . '%';
                    if ($first) {
                        $categoryQuery->where('name', 'LIKE', $category);
                        $first = false;
                    } else {
                        $categoryQuery->orWhere('name', 'LIKE', $category);
                    }
                }

                $categoryIds = $categoryQuery->pluck('id');

                $sq->where(function ($subQuery) use ($categoryIds) {
                    $subQuery->whereHas('categories', function ($q) use ($categoryIds) {
                        $q->whereIn('id', $categoryIds);
                    });
                });
            }

            if (!empty($tags)) {
                $tagQuery = PostTag::query();

                $first = true;
                foreach ($tags as $tag) {
                    $tag = '%' . trim(substr($tag, 1)) . '%';
                    if ($first) {
                        $tagQuery->where('name', 'LIKE', $tag);
                        $first = false;
                    } else {
                        $tagQuery->orWhere('name', 'LIKE', $tag);
                    }
                }

                $tagIds = $tagQuery->pluck('id');

                $sq->where(function ($subQuery) use ($tagIds) {
                    $subQuery->whereHas('tags', function ($q) use ($tagIds) {
                        $q->whereIn('id', $tagIds);
                    });
                });
            }
        });

        $query->where(function ($q) use ($searchWords, $searchColumns) {
            foreach ($searchWords as $term) {
                $term = '%' . trim($term) . '%';

                $q->orWhere(function ($subQuery) use ($term, $searchColumns) {
                    $first = true;
                    foreach ($searchColumns as $searchColumn) {
                        if ($first) {
                            $subQuery->where($searchColumn, 'LIKE', $term);
                            $first = false;
                        } else {
                            $subQuery->orWhere($searchColumn, 'LIKE', $term);
                        }
                    }
                });
            }
        });

        return $query;
    }

    /**
     * Specify how to build search query with filter keys and return result query.
     *
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function applyFilters($query, $filters)
    {
        foreach ($filters as $key => $ids) {
            if (empty($ids)) {
                continue;
            }
            switch ($key) {
                case 'categories':
                    $query->where(function ($subQuery) use ($ids) {
                        $subQuery->whereHas('categories', function ($q) use ($ids) {
                            $q->whereIn('id', $ids);
                        });
                    });
                    break;
                default:
                    break;
            }
        }

        return $query;
    }

    /**
     * Check if user with email already exists.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function exists()
    {
        if (request('slug')) {
            $slug = $this->class::withTrashed()->where('slug', request('slug'))->first();

            if ($slug) {
                return response('Error');
            }
        }
    }

    /**
     * Logs when a item is fetched.
     *
     * @param $item
     */
    public function beforeGet($item)
    {
        $item->tabs = ['info', 'content', 'source', 'images', 'files', 'seo', 'settings'];

        $item->log(Log::LOG_POST_VIEWED);
    }

    /**
     * Logs when a item was created.
     *
     * @param $item
     */
    public function afterCreate($item)
    {
        $item->log(Log::LOG_POST_CREATED);

        $item->getParts();

        Notification::notifyRoles('create' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('created')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was updated.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterUpdate($item, $changes)
    {
        $item->log(Log::LOG_POST_UPDATED);

        $item->getParts();

        if (isset($changes['enabled'])) {
            if ($changes['enabled']) {
                $item->log(Log::LOG_POST_ENABLED);
            } else {
                $item->log(Log::LOG_POST_DISABLED);
            }
        }

        $item->tabs = ['info', 'content', 'images', 'files', 'seo', 'settings'];

        Notification::notifyRoles('update' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('updated')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was deleted.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterDelete($item)
    {
        $item->log(Log::LOG_POST_DELETED);

        Notification::notifyRoles('delete' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('deleted')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was restored.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterRestore($item)
    {
        $item->log(Log::LOG_POST_RESTORED);

        Notification::notifyRoles('restore' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('restored')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Dispatches websocket events.
     *
     */
    public function dispatchEvents()
    {
        event(new CacheClear('postAuthors-store'));
        event(new CacheClear('postAuthors'));
        event(new UpdateDom('postAuthors'));
    }

    public function images()
    {
        $filter = request('filter');

        $results = $this->class::find(request('id'))->images();

        $controller = new ImagesController();

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $results = $controller->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (request('sortBy', null)) {
            $controller->checkSortBy($results);
        } else {
            $results->orderBy('id', 'desc');
        }

        return response()->json([
            __FUNCTION__ => $results->get()
        ]);
    }

    public function makeCover()
    {
        $post = $this->class::find(request('id'));
        foreach ($post->images()->where('details', 'like', '%isCover%')->get() as $image) {
            $details = $image->details;
            unset($details->isCover);
            $image->details = $details;
        }
        $image = $post->images()->where('id', request('image_id'))->first();
        if ($image) {
            $details = $image->details;
            $details->isCover = true;
            $image->details = $details;
        }
        return response()->json([
            'success' => true
        ]);
    }

    public function files()
    {
        $filter = request('filter');

        $results = $this->class::find(request('id'))->files();

        $controller = new FilesController();

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $results = $controller->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (request('sortBy', null)) {
            $controller->checkSortBy($results);
        } else {
            $results->orderBy('id', 'desc');
        }

        return response()->json([
            __FUNCTION__ => $results->paginate(request('itemsPerPage', 15)),
        ]);
    }

    public function hardDelete()
    {
        $i = $this->class::query()->withTrashed()->find(request($this->keyName));
        $i->forceDelete();

        if (method_exists($this, 'afterDelete')) {
            $this->afterDelete($i);
        }

        return response()->json([
            'message' => ucfirst($this->itemName) . ' deleted',
        ]);
    }
}
