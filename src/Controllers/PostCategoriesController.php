<?php

namespace Wai\AdminifyWysiwyg\Controllers;

use App\Log;
use App\Role;
use App\Notification;
use App\PostCategory;
use Illuminate\Support\Str;
use Wai\Adminify\Events\UpdateDom;
use Wai\Adminify\Events\CacheClear;
use Wai\Adminify\Controllers\VueController;

class PostCategoriesController extends VueController
{
    protected $class = PostCategory::class;
    protected $itemName = 'category';
    protected $orderAsc = false;

    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = ['thumbnail'];
    protected $multipleAppends = ['thumbnail', 'total_posts'];

    protected $singleRelationships = ['images'];
    protected $multipleRelationships = [];

    protected $searchColumns = ['name'];

    /**
     * Custom search query for users.
     *
     * @param bool $trashedOnly
     * @return mixed
     */
    public function searchQuery($trashedOnly = false)
    {
        $filter = request('filter');
        $itemsPerPage = request('itemsPerPage');

        if ($itemsPerPage) {
            $this->itemsPerPage = $itemsPerPage;
        }

        $results = $this->class::query();

        if ($trashedOnly) {
            $results->onlyTrashed();
        }

        $results->with($this->multipleRelationships);

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $this->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (isset($filter['filters']) and !empty($filter['filters'])) {
            $this->applyFilters($results, $filter['filters']);
        }

        $this->checkSortBy($results);

        return $results;
    }

    /**
     * Check if user with email already exists.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function exists()
    {
        if (request('slug')) {
            $slug = $this->class::findBySlug(request('slug'));

            if ($slug) {
                return response('Error');
            }
        }
    }

    /**
     * Logs when a item is fetched.
     *
     * @param $item
     */
    public function beforeGet($item)
    {
        $item->log(Log::LOG_POST_CATEGORY_VIEWED);
    }

    /**
     * Logs when a item was created.
     *
     * @param $item
     */
    public function afterCreate($item)
    {
        $item->log(Log::LOG_POST_CATEGORY_CREATED);

        Notification::notifyRoles('create' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('created')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was updated.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterUpdate($item)
    {
        $item->log(Log::LOG_POST_CATEGORY_UPDATED);

        Notification::notifyRoles('update' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('updated')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was deleted.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterDelete($item)
    {
        $item->log(Log::LOG_POST_CATEGORY_DELETED);

        Notification::notifyRoles('delete' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('deleted')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was restored.
     *
     * @param $item
     * @throws \Exception
     */
    public function afterRestore($item)
    {
        $item->log(Log::LOG_POST_CATEGORY_RESTORED);

        Notification::notifyRoles('restore' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('restored')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Dispatches websocket events.
     *
     */
    private function dispatchEvents()
    {
        event(new CacheClear('postCategories-store'));
        event(new CacheClear('postCategories'));
        event(new UpdateDom('postCategories'));
    }
}
