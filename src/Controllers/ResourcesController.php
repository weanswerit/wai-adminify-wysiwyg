<?php

namespace Wai\AdminifyWysiwyg\Controllers;

use App\PostAuthor;
use App\PostCategory;
use App\PostTag;

class ResourcesController
{
    protected $class = null;
    protected $itemName = null;

    /**
     * Returns post authors.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \ReflectionException
     */
    public function getPostAuthors()
    {
        return response()->json([
            $this->getResponseKey(__FUNCTION__) => self::getRaw(PostAuthor::class, [
                'id',
                'first_name',
                'last_name',
            ])
        ]);
    }

    /**
     * Returns post categories.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \ReflectionException
     */
    public function getPostCategories()
    {
        return response()->json([
            $this->getResponseKey(__FUNCTION__) => self::getRaw(PostCategory::class, [
                'id',
                'name',
            ])
        ]);
    }

    /**
     * Returns post tags.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \ReflectionException
     */
    public function getPostTags()
    {
        return response()->json([
            $this->getResponseKey(__FUNCTION__) => self::getRaw(PostTag::class, [
                'id',
                'name',
            ])
        ]);
    }

    /**
     * Make a direct query to database to fetch specific columns and ignores model.
     *
     * @param $model
     * @param array $columns
     * @param bool $deleted
     * @return mixed
     */
    public static function getRaw($model, $columns = [], $deleted = false)
    {
        $query = \DB::table((new $model)->getTable());

        if ($deleted) {
            $query->where('deleted_at', '=', null);
        }

        return $query->get($columns);
    }

    /**
     * If item name is set in parent function will return item name as response key.
     * Otherwise determines item name from function name.
     *
     * @param $functionName
     * @return mixed|null
     */
    public function getResponseKey($functionName)
    {
        if (is_null($this->itemName)) {
            $this->itemName = lcfirst(str_replace('get', '', $functionName));
        }

        return $this->itemName;
    }
}
