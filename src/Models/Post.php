<?php

namespace Wai\AdminifyWysiwyg\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    const IMAGES_DEFAULT = 1;
    const IMAGES_CAROUSEL = 2;
    const IMAGES_MASONRY = 3;

    const BLOG_LAYOUT_ONE = 1;
    const BLOG_LAYOUT_TWO = 2;

    protected $thumbnail_column = 'title';

    protected $dates = ['deleted_at'];

    protected $casts = [
        'enabled' => 'boolean',
        'featured' => 'boolean'
    ];

    protected $excludedImagesRoles = ['thumbnail'];

    /**
     * A post can belong to many categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function categories()
    {
        return $this->morphedByMany(\App\PostCategory::class, 'association', 'post_associations')
            ->select(['id', 'name', 'slug', 'details']);
    }

    /**
     * A post can have many authors.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function authors()
    {
        return $this->morphedByMany(\App\PostAuthor::class, 'association', 'post_associations')
            ->select(['id', 'first_name', 'last_name', 'slug', 'details']);
    }

    /**
     * A post can have many tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphedByMany(\App\PostTag::class, 'association', 'post_associations')
            ->where('enabled', true)
            ->select(['id', 'name', 'slug']);
    }

    public function getTagsAttribute()
    {
        return $this->tags()->get();
    }

    public function setTagsAttribute($tags)
    {
        $tagIds = [];

        foreach ($tags as $tag) {
            if (isset($tag['id'])) {
                $tagIds[] = $tag['id'];
                continue;
            }

            $dbTag = \App\PostTag::findBySlug(Str::slug($tag));

            if (!$dbTag) {
                $dbTag = \App\PostTag::create([
                    'name' => $tag,
                    'slug' => Str::slug($tag),
                    'enabled' => true,
                    'details' => [],
                ]);
            }

            $tagIds[] = $dbTag->id;
        }

        $this->tags()->sync($tagIds);
    }

    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    public function getDetailsAttribute()
    {
        $details = json_decode($this->attributes['details'], true);

        if (!isset($details['images_thumbnail_show'])) {
            $details['images_thumbnail_show'] = false;
        }

        if (!isset($details['images_exclude_cover'])) {
            $details['images_exclude_cover'] = true;
        }

        if (!isset($details['images_exclude_inline'])) {
            $details['images_exclude_inline'] = true;
        }

        if (!isset($details['images_layout'])) {
            $details['images_layout'] = self::IMAGES_DEFAULT;
        }

        if (!isset($details['blog_layout'])) {
            $details['blog_layout'] = self::BLOG_LAYOUT_ONE;
        }

        if (!isset($details['seo']['title'])) {
            $details['seo']['title'] = '';
        }

        if (!isset($details['seo']['description'])) {
            $details['seo']['description'] = '';
        }

        if (!isset($details['seo']['keywords'])) {
            $details['seo']['keywords'] = [];
        }

        return $details;
    }

    /**
     * Converting details from array to json to store in database.
     *
     * @param $value
     */
    public function setDetailsAttribute($value)
    {
        $details = !isset($this->attributes['details']) ? [] : json_decode($this->attributes['details'], true);

        foreach ($value as $p => $v) {
            $details[$p] = $v;
        }
        $this->attributes['details'] = json_encode($details);
    }

    /**
     * Finds a post by slug.
     *
     * @param $slug
     * @param array $appends
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public static function findBySlug($slug, $appends = [])
    {
        return self::query()->where('slug', $slug)->with($appends)->first();
    }

    public function setSlugAttribute($slug)
    {
        $this->attributes['slug'] = Str::slug($slug);
    }

    public function getAuthorsCountAttribute()
    {
        return $this->authors()->count();
    }

    public function setAuthorsCountAttribute()
    {
        return;
    }

    public function getCategoriesCountAttribute()
    {
        return $this->categories()->count();
    }

    public function setCategoriesCountAttribute()
    {
        return;
    }

    public function getTagsCountAttribute()
    {
        return $this->tags()->count();
    }

    public function setTagsCountAttribute()
    {
        return;
    }

    public function getAuthorIdsAttribute()
    {
        return $this->authors()->pluck('id');
    }

    public function setAuthorIdsAttribute($ids)
    {
        $this->authors()->sync($ids);
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories()->pluck('id');
    }

    public function setCategoryIdsAttribute($ids)
    {
        $this->categories()->sync($ids);
    }

    public function getTagIdsAttribute()
    {
        return $this->tags()->pluck('id');
    }

    public function setTagIdsAttribute($ids)
    {
        $this->tags()->sync($ids);
    }

    public function getPermissionsAttribute()
    {
        $permissions = [
            'can_view' => true, // temporary
            'can_edit' => false,
            'can_delete' => false,
        ];


        if (\Auth::user()->hasRole(\App\Role::ADMIN) or \Auth::user()->hasRole(\App\Role::SUPER_ADMIN)) {
            $permissions['can_edit'] = true;
            $permissions['can_delete'] = true;
        }

        return $permissions;
    }

    public function setPermissionsAttribute()
    {
        return;
    }

    public static function getImageLayouts()
    {
        return [
            [
                'id' => self::IMAGES_DEFAULT,
                'name' => 'Default'
            ],
            [
                'id' => self::IMAGES_CAROUSEL,
                'name' => 'Carousel'
            ],
            [
                'id' => self::IMAGES_MASONRY,
                'name' => 'Masonry'
            ]
        ];
    }

    public static function getPostLayouts()
    {
        return [
            [
                'id' => self::BLOG_LAYOUT_ONE,
                'name' => 'Layout 1'
            ],
            [
                'id' => self::BLOG_LAYOUT_TWO,
                'name' => 'Layout 2'
            ]
        ];
    }

    /**
     * Increment a post's hits by id and returns the new value
     * @param $id
     * @return mixed
     */
    public static function hitById($id)
    {
        $i = self::find($id);

        if ($i) {
            return $i->hit();
        }
    }

    /**
     * Increment a post's hits and returns the new value
     * @return int
     */
    public function hit()
    {
        $this->increment('hits');
        return (int)$this->hits;
    }

    public static function getPublishedQuery()
    {
        return self::query()
            ->where('enabled', true)
            ->where('publish_start', '<=', date('Y-m-d'))
            ->where(function ($q) {
                $q->whereNull('publish_end')
                    ->orWhere('publish_end', '>=', date('Y-m-d'));
            })
            ->orderBy('publish_start', 'DESC')->orderBy('id', 'DESC');
    }

    public static function getFeaturedQuery()
    {
        return self::getPublishedQuery()
            ->where('featured', true);
    }

    public function getThumbnail()
    {
        $firstImage = $this->images()->first();
        if ($firstImage) {
            return $firstImage;
        } else {
            return null;
        }
    }

    public function getParts()
    {
        $content = $this->content;

        // clearing usedInLine flags
        foreach ($this->images()->where('details', 'like', '%usedInline%')->get() as $image) {
            $details = $image->details;
            unset($details->usedInline);
            $image->details = $details;
        }

        $parts = [];
        $images = [];
        $media = [];

        preg_match_all('/<figure class="image( image-style-align-(.*))?"><img src="(.*)"( alt="(.*)")?>(<figcaption>(.*)<\/figcaption>)?<\/figure>/U', $content, $inlineImages);

        foreach ($inlineImages[0] as $index => $imgHtml) {
            $images[strpos($content, $imgHtml)] = [
                'html' => $imgHtml,
                'src' => $inlineImages[3][$index],
                'alt' => trim($inlineImages[5][$index]),
                'align' => $inlineImages[2][$index],
                'caption' => $inlineImages[7][$index],
            ];
        }

        $publicPattern = config('colortools.store.publicPattern');
        $publicPattern = '/' . ltrim(str_replace('%hash%', '', $publicPattern), '/');

        foreach ($images as $index => $image) {
            if (strpos($image['src'], $publicPattern) === 0) {
                $hash = substr($image['src'], strlen($publicPattern), 32);
                if (!(!empty($hash) and strlen($hash) == 32)) {
                    continue;
                }
                $imageStore = $this->images()->where('hash', $hash)->first();
                if ($imageStore) {
                    $details = $imageStore->details;
                    $details->usedInline = true;
                    $imageStore->details = $details;
                } else {
                    $imageStore = \App\ImageStore::getByHash($hash);
                }

                if ($imageStore) {
                    $image = array_merge($imageStore->toArray(), $image);
                    if (isset($image['metadata'])) {
                        unset($image['metadata']);
                    }
                    if (isset($image['src'])) {
                        unset($image['src']);
                    }

                    if (empty($image['alt'])) {
                        $image['alt'] = trim($image['name']);
                    }


                    $images[$index] = $image;
                }
            }
        }


        preg_match_all('/<figure class="media"><oembed url="(.*)"><\/oembed><\/figure>/U', $content, $inlineMedia);
        foreach ($inlineMedia[0] as $index => $mediaHtml) {
            $media[strpos($content, $mediaHtml)] = [
                'html' => $mediaHtml,
                'url' => $inlineMedia[1][$index],
                'type' => 'oembed',
            ];
        }

        $images = $images + $media;
        ksort($images);

        $after = $content;
        foreach ($images as $image) {
            $before = substr($after, 0, strpos($after, $image['html']));
            $after = substr($after, strpos($after, $image['html']) + strlen($image['html']));

            if (!empty($before)) {
                $parts[] = [
                    'type' => 'html',
                    'html' => $before
                ];
            }

            $parts[] = $image;
        }

        if (!empty($after)) {
            $parts[] = [
                'type' => 'html',
                'html' => $after
            ];
        }

        $this->parts = $parts;
        $this->save();

        return $parts;
    }

    /**
     * Converting parts column from json to array.
     *
     * @return mixed
     */
    public function getPartsAttribute()
    {
        return json_decode($this->attributes['parts'], true);
    }

    /**
     * Converting parts column from to json.
     *
     * @return mixed
     */
    public function setPartsAttribute($value)
    {
        return $this->attributes['parts'] = json_encode($value);
    }

    public function getGalleryAttribute()
    {
        $excludeInline = $this->details['images_exclude_inline'];
        $excludeCover = $this->details['images_exclude_cover'];

        $images = $this->images('images')->get();

        foreach ($images as $index => $image) {
            $images[$index]->show = true;
            $images[$index]->used_inline = false;
            if (isset($image->details->usedInline) and $image->details->usedInline) {
                $images[$index]->used_inline = true;

                if ($excludeInline) {
                    $images[$index]->show = false;
                }
            }


            if ($excludeCover && isset($image->details->isCover) and $image->details->isCover) {
                $images[$index]->show = false;
            }

        }

        return $images;
    }

    public function getCoverAttribute()
    {
        $image = $this->images()->where('details', 'like', '%isCover%')->first();
        if (!$image) {
            $image = $this->images()->first();
            return $image;
        }
        return $image;
    }

    public function setCoverAttribute($value)
    {
        return false;
    }

    public function getRelated($count = 3)
    {
        $fillersIds = self::getPublishedQuery()->where('featured', true)->where('id', '!=', $this->id)->take($count)->pluck('id')->toArray();

        $tagWeight = 8;
        $authorWeight = 5;
        $categoryWeight = 3;

        $scores = [];
        foreach ($this->tags as $tag) {
            foreach ($tag->posts()->where('id', '!=', $this->id)->take($count * 3)->pluck('id') as $postId) {
                if (!isset($scores[$postId])) {
                    $scores[$postId] = 0;
                }
                $scores[$postId] += $tagWeight + rand(3, 5);
            }
        }

        foreach ($this->authors as $author) {
            foreach ($author->posts()->where('id', '!=', $this->id)->take($count * 3)->pluck('id') as $postId) {
                if (!isset($scores[$postId])) {
                    $scores[$postId] = 0;
                }
                $scores[$postId] += $authorWeight;
            }
        }

        foreach ($this->categories as $category) {
            foreach ($category->posts()->where('id', '!=', $this->id)->take($count * 3)->pluck('id') as $postId) {
                if (!isset($scores[$postId])) {
                    $scores[$postId] = 0;
                }
                $scores[$postId] += $categoryWeight;
            }
        }

        arsort($scores);
        $matches = array_slice(array_keys($scores), 0, $count);

        if (count($matches) < $count) {
            $matches = array_slice(array_unique(array_merge($matches, $fillersIds)), 0, $count);
        }

        return self::whereIn('id', $matches);
    }

    public static function getSearchQuery($query = '')
    {
        $query = trim($query);

        $results = self::getPublishedQuery()
            ->orderBy('publish_start', 'desc');

        if (!empty($query)) {
            $results->where(function ($subQuery) use ($query) {
                $subQuery->where('title', 'like', '%' . $query . '%');
                $subQuery->orWhereHas('tags', function ($q) use ($query) {
                    $q->where('slug', 'like', '%' . $query . '%');
                });
                $subQuery->orWhereHas('authors', function ($q) use ($query) {
                    $q->where('first_name', 'like', '%' . $query . '%');
                    $q->orWhere('last_name', 'like', '%' . $query . '%');
                });
            });
        }

        return $results;
    }

}
