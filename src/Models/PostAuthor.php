<?php

namespace Wai\AdminifyWysiwyg\Models;

use FileTools\HasFiles;
use ColorTools\HasImages;
use Illuminate\Support\Str;
use Wai\Adminify\Traits\Loggable;
use Illuminate\Database\Eloquent\Model;
use Wai\Adminify\Traits\HasThumbnail;
use Wai\Adminify\Traits\DynamicChangeAppends;

class PostAuthor extends Model
{
    use Loggable;
    use HasFiles;
    use HasImages;
    use HasThumbnail;
    use DynamicChangeAppends;

    protected $thumbnail_column = 'first_name';

    protected $table = 'post_authors';

    protected $casts = [
        'enabled' => 'boolean'
    ];

    /**
     * An author can have many posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function posts()
    {
        return $this->morphToMany(\App\Post::class, 'association', 'post_associations', 'association_id', 'post_id');
    }

    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    public function getDetailsAttribute()
    {
        $details = json_decode($this->attributes['details'], true);

        if (!isset($details['description'])) {
            $details['description'] = '';
        }

        if (!isset($details['seo']['title'])) {
            $details['seo']['title'] = '';
        }

        if (!isset($details['seo']['description'])) {
            $details['seo']['description'] = '';
        }

        if (!isset($details['seo']['keywords'])) {
            $details['seo']['keywords'] = [];
        }

        return $details;
    }

    /**
     * Converting details from array to json to store in database.
     *
     * @param $value
     */
    public function setDetailsAttribute($value)
    {
        $details = !isset($this->attributes['details']) ? [] : json_decode($this->attributes['details'], true);

        foreach ($value as $p => $v) {
            $details[$p] = $v;
        }
        $this->attributes['details'] = json_encode($details);
    }

    /**
     * Finds a author by slug.
     *
     * @param $slug
     * @param array $appends
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public static function findBySlug($slug, $appends = [])
    {
        return self::query()->where('slug', $slug)->with($appends)->first();
    }

    public function setSlugAttribute($slug)
    {
        $this->attributes['slug'] = Str::slug($this->attributes['first_name'] . ' ' . $this->attributes['last_name']);
    }

    /**
     * Returns total amount of posts that belong to author.
     *
     * @return int
     */
    public function getTotalPostsAttribute()
    {
        return $this->posts()->count();
    }

    /**
     * Sets the total amount of posts that belong to author.
     *
     */
    public function setTotalPostsAttribute()
    {
        return;
    }

    public static function getPublishedQuery()
    {
        return self::getEnabledQuery()
            ->whereHas('posts');
    }

    public static function getEnabledQuery()
    {
        return self::query()
            ->where('enabled', true);
    }
}
